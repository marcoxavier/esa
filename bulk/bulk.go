package bulk

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/marcoxavier/esa/lib/elastic"

	"gitlab.com/marcoxavier/esa/lib/filesystem"

	"github.com/manifoldco/promptui"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// IndexName default index name.
	IndexName string
	// SourceFile file path.
	SourceFile string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "bulk",
		Short:   "Bulk updates",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&IndexName, "index", "", "default index for items which don't provide one")
	cmd.Flags().StringVar(&SourceFile, "file", "", "file path")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	var promptString = "Bulk Operation:\n" +
		"Source File:\t%s\n\n"

	cmd.Printf(promptString, SourceFile)

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Do you realy want to proceed"),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	bulkResponse, err := bulkUpdate(es)
	if err != nil {
		return errors.Wrap(err, "failed to bulk update")
	}

	if bulkResponse.Errors {
		cmd.Println("Some documents failed to copy:")
		for _, item := range bulkResponse.Items {
			for key, value := range item {
				if value.Error.Reason != "" {
					cmd.Printf("[%s] %s:\t%s\n", key, value.ID, value.Error.Reason)
				}
			}
		}
	}

	return nil
}

func bulkUpdate(es *elastic.Client) (bulk elastic.BulkUpdateResponse, _ error) {
	query, err := filesystem.LoadFile(SourceFile)
	if err != nil {
		return bulk, errors.Wrap(err, "failed to open file")
	}

	res, err := es.Bulk(
		bytes.NewReader(query),
		es.Bulk.WithIndex(IndexName),
	)
	if err != nil {
		return bulk, errors.Wrap(err, "failed bulk insert")
	}

	defer res.Body.Close()

	if res.IsError() {
		return bulk, errors.Wrap(fmt.Errorf(res.String()), "failed bulk insert")
	}

	var response elastic.BulkUpdateResponse
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return bulk, errors.Wrap(err, "failed to decode scroll response")
	}

	return bulk, nil
}
