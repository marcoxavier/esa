package tasks

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	"gitlab.com/marcoxavier/esa/tasks/get"
	errors "gitlab.com/vredens/go-errors"
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "tasks",
		Short: "Tasks api",
		RunE:  Run,
	}

	cmd.AddCommand(get.Command())

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	ses, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	res, err := ses.Tasks.List(
		ses.Tasks.List.WithHuman(),
		ses.Tasks.List.WithPretty(),
		ses.Tasks.List.WithWaitForCompletion(true),
	)
	if err != nil {
		return errors.Wrap(err, "failed to get task status by id")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "deletion operation completes with errors")
	}

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response")
	}

	_, _ = fmt.Fprintf(os.Stdout, "%s\n", string(raw))

	return nil
}
