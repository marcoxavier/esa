package get

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// ID task identifier.
	ID string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "get",
		Short: "Gets task status by id",
		RunE:  Run,
	}

	cmd.Flags().StringVar(&ID, "id", "", "task id")

	_ = cmd.MarkFlagRequired("id")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	ses, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	raw, err := ses.GetTaskRaw(context.Background(), ID)
	if err != nil {
		return err
	}

	_, _ = fmt.Fprintf(os.Stdout, "%s\n", string(raw))

	return nil
}
