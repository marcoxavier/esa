package infrastructure

import (
	"os"

	"github.com/spf13/viper"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// SessionConfiguration is a reporter session configuration.
	SessionConfiguration Configuration
)

// Configuration is the esa configuration.
type Configuration struct {
	Host     string `mapstructure:"host" validate:"nonzero"`
	ReadOnly bool   `mapstructure:"read_only"`
}

// LoadConfiguration loads the appropriate configuration.
// Returns the path to the configuration file used.
func LoadConfiguration() (config map[string]Configuration, _ string, _ error) {
	homeDir, _ := os.UserHomeDir()

	v := viper.New()
	v.SetConfigName(".config")
	v.SetConfigType("toml")
	v.AddConfigPath("/etc/esa")
	v.AddConfigPath(homeDir + "/esa")
	v.AddConfigPath(".")

	if err := v.ReadInConfig(); err != nil {
		return config, "", errors.Wrap(err, "failed to read in configuration")
	}

	if err := v.Unmarshal(&config); err != nil {
		return config, v.ConfigFileUsed(), errors.Wrap(err, "failed to unmarshal file contents into the configuration structure")
	}

	return config, v.ConfigFileUsed(), nil
}

// LoadConfigurationFromFile loads the appropriate configuration from a specific path.
// Returns the path to the configuration file used.
func LoadConfigurationFromFile(file string) (config map[string]Configuration, _ string, _ error) {
	v := viper.New()
	v.SetConfigType("toml")
	v.SetConfigFile(file)

	if err := v.ReadInConfig(); err != nil {
		return config, "", errors.Wrap(err, "failed to read in configuration")
	}

	if err := v.Unmarshal(&config); err != nil {
		return config, v.ConfigFileUsed(), errors.Wrap(err, "failed to unmarshal file contents into the configuration structure")
	}

	return config, v.ConfigFileUsed(), nil
}
