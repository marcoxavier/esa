package infrastructure

import (
	"gitlab.com/marcoxavier/esa/lib/elastic"
)

// NewElasticSearchClient return an elasticsearch client.
func NewElasticSearchClient(host string) (*elastic.Client, error) {
	return elastic.NewClient(elastic.ClientConfig{Addresses: []string{host}})
}
