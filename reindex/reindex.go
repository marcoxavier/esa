package reindex

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/schollz/progressbar/v2"

	"gitlab.com/marcoxavier/esa/lib/elastic"

	"github.com/manifoldco/promptui"

	"gitlab.com/marcoxavier/esa/lib/filesystem"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

const (
	reindexQuery = `
{
	"source": {
		"index": "%s",
		"query": %s
	},
     "dest": {
          "index": "%s"
     }
}
`

	reindexWithScriptQuery = `
{
	"source": {
		"index": "%s",
		"query": %s
	},
     "dest": {
          "index": "%s"
     },
	"script": {
          "source": %s,
          "lang": "painless"
	}
}
`

	reindexRemoteQuery = `
{
	"source": {
		"remote": {
	          "host": "%s"
		},
		"index": "%s",
		"query": %s
	},
     "dest": {
          "index": "%s"
     }
}
`

	reindexRemoteWithScriptQuery = `
{
	"source": {
		"remote": {
	          "host": "%s"
		},
		"index": "%s",
		"query": %s
	},
     "dest": {
          "index": "%s"
     },
	"script": {
          "source": %s,
          "lang": "painless"
	}
}
`
)

var (
	// SourceIndex source index name.
	SourceIndex string
	// DestinationIndex destination index name.
	DestinationIndex string
	// SourceHost source cluster host.
	SourceHost string
	// ScriptPath script path.
	ScriptPath string
	// Query query.
	Query string
	// SkipOnFailure skip on failure.
	SkipOnFailure bool
)

type reindexTaskStatusResponse struct {
	Total    int               `json:"total"`
	Updated  int               `json:"updated"`
	Created  int               `json:"created"`
	Deleted  int               `json:"deleted"`
	Noops    int               `json:"noops"`
	Failures []json.RawMessage `json:"failures"`
}

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "reindex",
		Short:   "Starts reindex script",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&SourceIndex, "src-index", "", "source index name")
	cmd.Flags().StringVar(&DestinationIndex, "dst-index", "", "destination index name, if not provided, will be the same value as source index name")
	cmd.Flags().StringVar(&SourceHost, "src-host", "", "source es cluster host")
	cmd.Flags().StringVar(&ScriptPath, "script", "", "optional painless script path")
	cmd.Flags().StringVar(&Query, "query", "", "optional query")
	cmd.Flags().BoolVar(&SkipOnFailure, "skip-on-failure", false, "reindex continues on reindex failure")

	_ = cmd.MarkFlagRequired("src-index")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	if SourceHost == infrastructure.SessionConfiguration.Host {
		return fmt.Errorf("source elasticsearch cluster host is the same as configured host")
	}

	var promptString = "Reindex Operation:\n" +
		"From:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n" +
		"To:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n" +
		"Script:\t\t%s\n\n"

	cmd.Printf(promptString, SourceIndex, SourceHost, DestinationIndex, infrastructure.SessionConfiguration.Host, ScriptPath)

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Do you realy want to proceed"),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	if Query == "" {
		Query = `{ "match_all": {} }`
	}

	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	var query string
	if ScriptPath == "" {
		if SourceHost != "" {
			query = fmt.Sprintf(reindexRemoteQuery, SourceHost, SourceIndex, Query, DestinationIndex)
		} else {
			query = fmt.Sprintf(reindexQuery, SourceIndex, Query, DestinationIndex)
		}
	} else {
		script, err := filesystem.LoadFile(ScriptPath)
		if err != nil {
			return errors.Wrap(err, "failed to open script")
		}

		scriptRaw, err := json.Marshal(string(script))
		if err != nil {
			return errors.Wrap(err, "failed to marshal script")
		}

		if SourceHost != "" {
			query = fmt.Sprintf(reindexRemoteWithScriptQuery, SourceHost, SourceIndex, Query, DestinationIndex, string(scriptRaw))
		} else {
			query = fmt.Sprintf(reindexWithScriptQuery, SourceIndex, Query, DestinationIndex, string(scriptRaw))
		}
	}

	cmd.Println("reindexing")
	res, err := es.Reindex(
		strings.NewReader(query),
		es.Reindex.WithWaitForCompletion(false),
		es.Reindex.WithHuman(),
		es.Reindex.WithPretty(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to request reindex operation")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "reindex operation completes with errors")
	}

	var waitResponse elastic.WaitForCompletionResponse
	if err := json.NewDecoder(res.Body).Decode(&waitResponse); err != nil {
		return errors.Wrap(err, "failed to decode response")
	}

	pb := progressbar.NewOptions(
		1000,
		progressbar.OptionSetPredictTime(false),
	)

	cmd.Printf("watching task: %s\n", waitResponse.Task)

	return es.WatchTask(context.Background(), elastic.WatchTaskOptions{ID: waitResponse.Task}, func(response elastic.TasksGetResponse) error {
		var status reindexTaskStatusResponse
		_ = response.GetStatus(&status)
		pb.ChangeMax(status.Total)
		pb.Reset()

		if len(status.Failures) > 0 && !SkipOnFailure {
			cmd.Printf("\n")
			for _, failure := range status.Failures {
				cmd.Printf("%s\n", string(failure))
			}
			return errors.New("reindex ended with errors")
		}

		if response.Completed {
			_ = pb.Finish()
			if response.HasErrors() {
				cmd.Printf("\n%s\n\n", string(response.Errors))
				return errors.New("task ended with errors")
			}

			cmd.Printf("\n%s\n\n", string(response.Response))
			return nil
		}

		_ = pb.Add(status.Created + status.Updated + status.Deleted + status.Noops)

		return nil
	})
}
