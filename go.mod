module gitlab.com/marcoxavier/esa

go 1.12

require (
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20190625055212-2870f808ff85
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/manifoldco/promptui v0.3.2
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pelletier/go-toml v1.3.0 // indirect
	github.com/schollz/progressbar/v2 v2.14.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.3.2
	github.com/valyala/fastjson v1.4.1
	gitlab.com/vredens/go-errors v0.3.0
	gitlab.com/vredens/go-logger/v2 v2.0.1
	golang.org/x/sys v0.0.0-20190403152447-81d4e9dc473e // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
	gopkg.in/validator.v2 v2.0.0-20180514200540-135c24b11c19
)
