package copy

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/marcoxavier/esa/lib/elastic"

	"gitlab.com/marcoxavier/esa/bulk"

	"gitlab.com/marcoxavier/esa/reindex"

	"github.com/manifoldco/promptui"

	"github.com/spf13/cobra"
	"github.com/valyala/fastjson"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

const (
	copyBulkScriptRow = `{ "update" : {"_id" : "%s", "_type": "_doc", "retry_on_conflict" : 3} }
{ "doc" : %s, "doc_as_upsert" : true }
`
)

var (
	// SourceIndex source index name.
	SourceIndex string
	// DestinationIndex destination index name.
	DestinationIndex string
	// SourceHost source cluster host.
	SourceHost string
	// Query query.
	Query string
)

type document struct {
	ID     string          `json:"_id"`
	Source json.RawMessage `json:"_source"`
}

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "copy",
		Short:   "Copies documents between indexes",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&SourceIndex, "src-index", "", "source index name")
	cmd.Flags().StringVar(&DestinationIndex, "dst-index", "", "destination index name, if not provided, will be the same value as source index name")
	cmd.Flags().StringVar(&SourceHost, "src-host", "", "source es cluster host")
	cmd.Flags().StringVar(&Query, "query", "", "optional query")

	_ = cmd.MarkFlagRequired("src-index")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if DestinationIndex == "" {
		DestinationIndex = SourceIndex
	}

	reindex.DestinationIndex = DestinationIndex
	reindex.SourceHost = SourceHost
	reindex.SourceIndex = SourceIndex
	reindex.Query = Query

	bulk.IndexName = DestinationIndex

	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	if SourceHost == infrastructure.SessionConfiguration.Host {
		return fmt.Errorf("source elasticsearch cluster host is the same as configured host")
	}

	var promptString = "Copy Operation:\n" +
		"From:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n" +
		"To:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n\n"

	cmd.Printf(promptString, SourceIndex, SourceHost, DestinationIndex, infrastructure.SessionConfiguration.Host)

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Do you realy want to proceed"),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	if SourceHost == "" {
		return reindex.Run(cmd, args)
	}

	res, err := es.Cluster.GetSettings(
		es.Cluster.GetSettings.WithIncludeDefaults(true),
		es.Cluster.GetSettings.WithHuman(),
		es.Cluster.GetSettings.WithPretty(),
		es.Cluster.GetSettings.WithFilterPath("defaults.reindex.remote.whitelist"),
	)

	if err != nil {
		return errors.Wrap(err, "failed to getting cluster settings")
	}

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "failed getting cluster settings")
	}

	resRaw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read setting response")
	}

	var p fastjson.Parser
	v, err := p.ParseBytes(resRaw)
	if err != nil {
		return errors.Wrap(err, "failed to parse response")
	}

	if strings.Contains(string(v.GetStringBytes("defaults.reindex.remote.whitelist")), `"`+SourceHost+`"`) {
		return reindex.Run(cmd, args)
	}

	return reindexByBulk(cmd, args)
}

func reindexByBulk(cmd *cobra.Command, args []string) error {
	ses, err := infrastructure.NewElasticSearchClient(SourceHost)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	scroll := ses.NewScroll(SourceIndex, elastic.ScrollOptions{Size: 1000})

	var totalProcessed int
	for scroll.Next() {
		response, err := scroll.GetResponse()
		if err != nil {
			return errors.Wrap(err, "failed to get scroll response")
		}

		if err := bulkUpdate(response.Hits.Hits, cmd, args); err != nil {
			return err
		}

		totalProcessed += len(response.Hits.Hits)
		cmd.Printf("processed: %d / %d\n", totalProcessed, response.Hits.Total)
	}

	return nil
}

func bulkUpdate(docs []elastic.DocumentResponse, cmd *cobra.Command, args []string) error {
	f, err := ioutil.TempFile(os.TempDir(), "")
	if err != nil {
		return errors.Wrap(err, "failed to create tmp file")
	}
	defer f.Close()
	defer os.Remove(f.Name())

	for _, doc := range docs {
		if _, err := f.WriteString(fmt.Sprintf(copyBulkScriptRow, doc.ID, string(doc.Source))); err != nil {
			return errors.Wrap(err, "failed to write file")
		}
	}

	bulk.SourceFile = f.Name()

	return bulk.Run(cmd, args)
}
