package watcher

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/marcoxavier/esa/lib/elastic"

	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"

	"gitlab.com/marcoxavier/esa/tasks/get"

	"github.com/spf13/cobra"
)

var (
	// ID task identifier.
	ID string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "watcher",
		Short: "Watches task by id",
		RunE:  Run,
	}

	cmd.Flags().StringVar(&ID, "id", "", "task id")

	_ = cmd.MarkFlagRequired("id")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	// listen for termination signals
	termChan := make(chan os.Signal, 1)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
	get.ID = ID

	ses, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	return ses.WatchTask(context.Background(), elastic.WatchTaskOptions{ID: ID}, func(response elastic.TasksGetResponse) error {
		if response.Completed {
			if response.HasErrors() {
				cmd.Printf("errors: %s\n\n", string(response.Errors))
				return errors.New("task ended with errors")
			}

			cmd.Printf("response: %s\n\n", string(response.Response))
			return nil
		}

		cmd.Printf("\rstatus: %s\n\n", string(response.Task.Status))

		return nil
	})
}
