package task

import (
	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/custom/task/watcher"
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "task",
		Short: "Task custom command",
	}

	cmd.AddCommand(watcher.Command())

	return cmd
}
