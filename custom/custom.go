package custom

import (
	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/custom/copy"
	"gitlab.com/marcoxavier/esa/custom/reindex"
	"gitlab.com/marcoxavier/esa/custom/task"
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "custom",
		Short: "Custom commands",
	}

	cmd.AddCommand(reindex.Command())
	cmd.AddCommand(copy.Command())
	cmd.AddCommand(task.Command())

	return cmd
}
