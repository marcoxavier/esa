package reindex

import (
	"fmt"
	"strings"

	"gitlab.com/marcoxavier/esa/indices/alias/move"

	"gitlab.com/marcoxavier/esa/indices/create"

	"gitlab.com/marcoxavier/esa/reindex"

	"github.com/manifoldco/promptui"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

var (
	// SourceIndex source index name.
	SourceIndex string
	// DestinationIndex destination index name.
	DestinationIndex string
	// SourceHost source cluster host.
	SourceHost string
	// ScriptPath script path.
	ScriptPath string
	// SettingsPath setting path.
	SettingsPath string
	// AliasToMove alias name.
	AliasToMove string
	// Query query
	Query string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "reindex",
		Short:   "Starts custom reindex script",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&SourceIndex, "src-index", "", "source index name")
	cmd.Flags().StringVar(&DestinationIndex, "dst-index", "", "destination index name, if not provided, will be the same value as source index name")
	cmd.Flags().StringVar(&SourceHost, "src-host", "", "source es cluster host")
	cmd.Flags().StringVar(&ScriptPath, "script", "", "optional painless script path")
	cmd.Flags().StringVar(&AliasToMove, "alias", "", "alias to move")
	cmd.Flags().StringVar(&SettingsPath, "settings", "", "setting and mappings configuration path")
	cmd.Flags().StringVar(&Query, "query", "", "optional query")

	_ = cmd.MarkFlagRequired("src-index")
	_ = cmd.MarkFlagRequired("dst-index")
	_ = cmd.MarkFlagRequired("alias")
	_ = cmd.MarkFlagRequired("settings")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if DestinationIndex == "" {
		DestinationIndex = SourceIndex
	}

	reindex.DestinationIndex = DestinationIndex
	reindex.SourceHost = SourceHost
	reindex.SourceIndex = SourceIndex
	reindex.ScriptPath = ScriptPath
	reindex.Query = Query

	create.IndexName = DestinationIndex
	create.SettingsPath = SettingsPath

	move.Alias = AliasToMove
	move.FromIndex = SourceIndex
	move.ToIndex = DestinationIndex

	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	var promptString = "Reindex Operation:\n" +
		"From:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n" +
		"To:\t\tIndex:\t%s\n" +
		"\t\tHost:\t%s\n" +
		"Alias:\t\t%s\n" +
		"Mappings:\t%s\n" +
		"Script:\t\t%s\n\n"

	cmd.Printf(promptString, SourceIndex, SourceHost, DestinationIndex, infrastructure.SessionConfiguration.Host, AliasToMove, SettingsPath, ScriptPath)

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Do you realy want to proceed"),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	if err := create.Run(cmd, args); err != nil {
		return err
	}

	if err := reindex.Run(cmd, args); err != nil {
		return err
	}

	if err := move.Run(cmd, args); err != nil {
		return err
	}

	return nil
}
