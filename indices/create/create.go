package create

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/manifoldco/promptui"

	"gitlab.com/marcoxavier/esa/lib/filesystem"

	errors "gitlab.com/vredens/go-errors"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

var (
	// SettingsPath setting path.
	SettingsPath string
	// IndexName index name.
	IndexName string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "create",
		Short:   "Creates index with a specific settings",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&SettingsPath, "settings", "", "setting and mappings configuration path")
	_ = cmd.MarkPersistentFlagRequired("index")
	_ = cmd.MarkFlagRequired("settings")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	IndexName, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Create index [%s] with configuration [%s]", IndexName, SettingsPath),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs entity feeder.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	mapping, err := filesystem.LoadFile(SettingsPath)
	if err != nil {
		return errors.Wrap(err, "failed to open index settings")
	}

	cmd.Println("creating index")
	res, err := es.Indices.Create(
		IndexName,
		es.Indices.Create.WithBody(bytes.NewReader(mapping)),
		es.Indices.Create.WithHuman(),
		es.Indices.Create.WithPretty(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to create index")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "create index operation completes with errors")
	}

	cmd.Println(res)

	return nil
}
