package create

import (
	"fmt"
	"strings"

	"github.com/manifoldco/promptui"

	errors "gitlab.com/vredens/go-errors"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

var (
	// Alias alias name.
	Alias string
	// IndexName index name.
	IndexName string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "create",
		Short:   "Creates alias",
		RunE:    Run,
		PreRunE: PreRun,
	}

	_ = cmd.MarkPersistentFlagRequired("alias")
	_ = cmd.MarkPersistentFlagRequired("index")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	Alias, err = cmd.Flags().GetString("alias")
	if err != nil {
		return err
	}

	IndexName, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Create alias [%s] in [%s]", Alias, IndexName),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	cmd.Println("creating alias")
	res, err := es.Indices.PutAlias(
		[]string{IndexName}, Alias,
		es.Indices.PutAlias.WithHuman(),
		es.Indices.PutAlias.WithPretty(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to create alias")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "create alias operation completes with errors")
	}

	cmd.Println(res)

	return nil
}
