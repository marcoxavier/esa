package alias

import (
	"io/ioutil"

	"gitlab.com/marcoxavier/esa/indices/alias/create"
	"gitlab.com/marcoxavier/esa/indices/alias/delete"
	"gitlab.com/marcoxavier/esa/indices/alias/move"

	"github.com/elastic/go-elasticsearch/v8/esapi"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// Alias alias name.
	Alias string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "alias",
		Short: "List alias",
		RunE:  Run,
	}

	cmd.AddCommand(delete.Command())
	cmd.AddCommand(create.Command())
	cmd.AddCommand(move.Command())

	cmd.PersistentFlags().StringVar(&Alias, "alias", "", "alias name")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	nameOpt := dumbOption()
	if Alias != "" {
		nameOpt = es.Cat.Aliases.WithName(Alias)
	}

	res, err := es.Cat.Aliases(
		nameOpt,
		es.Cat.Aliases.WithV(true),
		es.Cat.Aliases.WithHuman(),
		es.Cat.Aliases.WithPretty(),
		es.Cat.Aliases.WithS("alias"),
	)
	if err != nil {
		return errors.Wrap(err, "failed to cat aliases")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "cat alias operation completes with errors")
	}

	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response body")
	}

	cmd.Println(string(raw))

	return nil
}

func dumbOption() func(*esapi.CatAliasesRequest) {
	return func(r *esapi.CatAliasesRequest) {}
}
