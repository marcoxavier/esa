package move

import (
	"fmt"
	"strings"

	"github.com/manifoldco/promptui"

	errors "gitlab.com/vredens/go-errors"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

const (
	moveAliasQuery = `
{
  "actions":[
    { "remove": { "index": "%s", "alias": "%s" } },
    { "add": { "index": "%s", "alias": "%s" } } 
  ]
}
`
)

var (
	// Alias alias name.
	Alias string
	// FromIndex from index name.
	FromIndex string
	// ToIndex to index name.
	ToIndex string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "move",
		Short:   "Moves alias",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&ToIndex, "to", "", "index name where alias will be moved")
	_ = cmd.MarkPersistentFlagRequired("alias")
	_ = cmd.MarkPersistentFlagRequired("index")
	_ = cmd.MarkFlagRequired("to")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	Alias, err = cmd.Flags().GetString("alias")
	if err != nil {
		return err
	}

	FromIndex, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Move alias [%s] from [%s] to [%s]", Alias, FromIndex, ToIndex),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	body := fmt.Sprintf(moveAliasQuery, FromIndex, Alias, ToIndex, Alias)

	cmd.Println("moving alias")
	res, err := es.Indices.UpdateAliases(
		strings.NewReader(body),
		es.Indices.UpdateAliases.WithHuman(),
		es.Indices.UpdateAliases.WithPretty(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to move alias")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "moving alias operation completes with errors")
	}

	cmd.Println(res)

	return nil
}
