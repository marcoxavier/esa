package indices

import (
	"io/ioutil"

	"gitlab.com/marcoxavier/esa/indices/scroll"

	"gitlab.com/marcoxavier/esa/indices/get"

	"gitlab.com/marcoxavier/esa/indices/create"
	"gitlab.com/marcoxavier/esa/indices/delete"

	"gitlab.com/marcoxavier/esa/indices/mapping"

	"gitlab.com/marcoxavier/esa/indices/update"

	"gitlab.com/marcoxavier/esa/indices/alias"

	"github.com/elastic/go-elasticsearch/v8/esapi"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// IndexName index name.
	IndexName string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "indices",
		Short: "List indices",
		RunE:  Run,
	}

	cmd.AddCommand(alias.Command())
	cmd.AddCommand(update.Command())
	cmd.AddCommand(mapping.Command())
	cmd.AddCommand(create.Command())
	cmd.AddCommand(delete.Command())
	cmd.AddCommand(get.Command())
	cmd.AddCommand(scroll.Command())

	cmd.PersistentFlags().StringVar(&IndexName, "index", "", "index name")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	nameOpt := dumbOption()
	if IndexName != "" {
		nameOpt = es.Cat.Indices.WithIndex(IndexName)
	}

	res, err := es.Cat.Indices(
		nameOpt,
		es.Cat.Indices.WithV(true),
		es.Cat.Indices.WithHuman(),
		es.Cat.Indices.WithPretty(),
		es.Cat.Indices.WithH("health", "status", "index", "uuid", "docs.count", "docs.deleted", "pri.store.size", "pri.memory.total", "segments.memory"),
		es.Cat.Indices.WithS("index"),
	)
	if err != nil {
		return errors.Wrap(err, "failed to cat indexes")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return nil
	}

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response body")
	}

	cmd.Println(string(raw))

	return nil
}

func dumbOption() func(*esapi.CatIndicesRequest) {
	return func(r *esapi.CatIndicesRequest) {}
}
