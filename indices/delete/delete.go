package delete

import (
	"fmt"
	"strings"

	"github.com/manifoldco/promptui"

	errors "gitlab.com/vredens/go-errors"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

var (
	// IndexName index name.
	IndexName string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "delete",
		Short:   "Deletes an index",
		RunE:    Run,
		PreRunE: PreRun,
	}

	_ = cmd.MarkPersistentFlagRequired("index")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	IndexName, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Delete index [%s]", IndexName),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs entity feeder.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	cmd.Println("running installation")
	res, err := es.Indices.Delete(
		[]string{IndexName},
		es.Indices.Delete.WithHuman(),
		es.Indices.Delete.WithPretty(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to delete index")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "deletion operation completes with errors")
	}

	cmd.Println(res)

	return nil
}
