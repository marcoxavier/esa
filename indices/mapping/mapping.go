package mapping

import (
	"io/ioutil"
	"strings"

	"github.com/elastic/go-elasticsearch/v8/esapi"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// IndexName index name.
	IndexName string
	// Fields fields to cat separated by comma.
	Fields string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "mapping",
		Short:   "Lists mapping to a specific index",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&Fields, "fields", "", "documents fields separated by comma")
	_ = cmd.MarkPersistentFlagRequired("index")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	IndexName, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	return nil
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	var res *esapi.Response
	if Fields != "" {
		res, err = es.Indices.GetFieldMapping(
			strings.Split(Fields, ","),
			es.Indices.GetFieldMapping.WithIndex(IndexName),
			es.Indices.GetFieldMapping.WithHuman(),
			es.Indices.GetFieldMapping.WithPretty(),
		)
	} else {
		res, err = es.Indices.GetMapping(
			es.Indices.GetMapping.WithIndex(IndexName),
			es.Indices.GetMapping.WithHuman(),
			es.Indices.GetMapping.WithPretty(),
		)
	}

	if err != nil {
		return errors.Wrap(err, "failed to get mappings")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "cat mapping operation completes with errors")
	}

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response body")
	}

	cmd.Println(string(raw))

	return nil
}
