package update

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/schollz/progressbar/v2"
	"gitlab.com/marcoxavier/esa/lib/elastic"

	"github.com/elastic/go-elasticsearch/v8/esapi"

	"github.com/manifoldco/promptui"

	"gitlab.com/marcoxavier/esa/lib/filesystem"

	errors "gitlab.com/vredens/go-errors"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
)

const (
	updateScriptQuery = `
{
	"script": {
		"source": %s,
		"lang": "painless"
	}
}
`
)

var (
	// ScriptPath script path.
	ScriptPath string
	// IndexName index name.
	IndexName string
	// ProceedOnConflict option to proceed on version conflict.
	ProceedOnConflict bool
)

type updateTaskStatusResponse struct {
	Total   int `json:"total"`
	Updated int `json:"updated"`
	Created int `json:"created"`
	Deleted int `json:"deleted"`
	Noops   int `json:"noops"`
}

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "update",
		Short:   "Updates index running painless script",
		RunE:    Run,
		PreRunE: PreRun,
	}

	cmd.Flags().StringVar(&ScriptPath, "script", "", "painless script path")
	cmd.Flags().BoolVar(&ProceedOnConflict, "proceed-on-conflict", false, "option to proceed on version conflict")
	_ = cmd.MarkPersistentFlagRequired("index")
	_ = cmd.MarkFlagRequired("script")

	return cmd
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) (err error) {
	if infrastructure.SessionConfiguration.ReadOnly {
		return fmt.Errorf("can't perform this action, service is configured as read only")
	}

	IndexName, err = cmd.Flags().GetString("index")
	if err != nil {
		return err
	}

	prompt := promptui.Prompt{
		Label:     fmt.Sprintf("Update index [%s]", IndexName),
		IsConfirm: true,
	}

	result, _ := prompt.Run()
	if strings.ToLower(result) != "y" {
		return fmt.Errorf("operation aborted")
	}

	return nil
}

// Run runs entity feeder.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	script, err := filesystem.LoadFile(ScriptPath)
	if err != nil {
		return errors.Wrap(err, "failed to open script")
	}

	scriptRaw, err := json.Marshal(string(script))
	if err != nil {
		return errors.Wrap(err, "failed to marshal script")
	}

	body := fmt.Sprintf(updateScriptQuery, string(scriptRaw))

	opts := []func(*esapi.UpdateByQueryRequest){
		es.UpdateByQuery.WithBody(strings.NewReader(body)),
		es.UpdateByQuery.WithHuman(),
		es.UpdateByQuery.WithPretty(),
		es.UpdateByQuery.WithWaitForCompletion(false),
	}

	if ProceedOnConflict {
		opts = append(opts, es.UpdateByQuery.WithConflicts("proceed"))
	}

	cmd.Println("running update")
	res, err := es.UpdateByQuery(
		[]string{IndexName},
		opts...,
	)
	if err != nil {
		return errors.Wrap(err, "failed to update index")
	}

	defer res.Body.Close()

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "update operation completes with errors")
	}

	var waitResponse elastic.WaitForCompletionResponse
	if err := json.NewDecoder(res.Body).Decode(&waitResponse); err != nil {
		return errors.Wrap(err, "failed to decode response")
	}

	pb := progressbar.NewOptions(
		1000,
		progressbar.OptionSetPredictTime(false),
	)
	defer cmd.Printf("\n\n")

	cmd.Printf("watching task: %s\n", waitResponse.Task)

	return es.WatchTask(context.Background(), elastic.WatchTaskOptions{ID: waitResponse.Task}, func(response elastic.TasksGetResponse) error {
		var status updateTaskStatusResponse
		_ = response.GetStatus(&status)
		pb.ChangeMax(status.Total)
		pb.Reset()

		if response.Completed {
			_ = pb.Finish()
			if response.HasErrors() {
				cmd.Printf("\n%s\n\n", string(response.Errors))
				return errors.New("task ended with errors")
			}

			cmd.Printf("\n%s\n\n", string(response.Response))
			return nil
		}

		_ = pb.Add(status.Created + status.Updated + status.Deleted + status.Noops)

		return nil
	})
}
