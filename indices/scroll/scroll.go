package scroll

import (
	"fmt"
	"os"

	"gitlab.com/marcoxavier/esa/lib/elastic"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// IndexName index name.
	IndexName string
	// Count number of hits returns.
	Count int
	// Query query
	Query string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "scroll",
		Short: "Scrolls through index",
		RunE:  Run,
	}

	cmd.Flags().StringVar(&IndexName, "index", "", "index name")
	cmd.Flags().StringVarP(&Query, "query", "q", "", "optional query")
	cmd.Flags().IntVarP(&Count, "number", "n", 0, "number of returned records")

	_ = cmd.MarkFlagRequired("index")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	ses, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	scroll := ses.NewScroll(IndexName, elastic.ScrollOptions{Size: 1000, Query: Query})

	var totalPrinted = 0
	for scroll.Next() {
		response, err := scroll.GetResponse()
		if err != nil {
			return errors.Wrap(err, "failed to get scroll response")
		}

		for _, hit := range response.Hits.Hits {
			_, _ = fmt.Fprintf(os.Stdout, "%s\n", string(hit.Source))
			totalPrinted++

			if Count > 0 && totalPrinted >= Count {
				return nil
			}
		}
	}

	return nil
}
