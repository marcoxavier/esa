package get

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
)

var (
	// IndexName index name.
	IndexName string
	// ID document identifier.
	ID string
)

// Command creates cobra command.
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "get",
		Short: "gets document source by id",
		RunE:  Run,
	}

	cmd.Flags().StringVar(&IndexName, "index", "", "index name")
	cmd.Flags().StringVar(&ID, "id", "", "document id")

	_ = cmd.MarkFlagRequired("index")
	_ = cmd.MarkFlagRequired("id")

	return cmd
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	ses, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	res, err := ses.GetSource(
		IndexName,
		ID,
	)
	if err != nil {
		return errors.Wrap(err, "failed to get document source")
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusNotFound {
		_, _ = fmt.Fprint(os.Stdout, "not found")
		return nil
	}

	if res.IsError() {
		cmd.Println(res)
		return errors.Wrap(err, "deletion operation completes with errors")
	}

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response")
	}

	_, _ = fmt.Fprintf(os.Stdout, "%s\n", string(raw))

	return nil
}
