package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"time"

	"gitlab.com/marcoxavier/esa/tasks"

	"gitlab.com/marcoxavier/esa/bulk"

	"gitlab.com/marcoxavier/esa/custom"

	"gitlab.com/marcoxavier/esa/reindex"

	"gitlab.com/marcoxavier/esa/indices"

	validator "gopkg.in/validator.v2"

	"github.com/spf13/cobra"
	"gitlab.com/marcoxavier/esa/infrastructure"
	errors "gitlab.com/vredens/go-errors"
	logger "gitlab.com/vredens/go-logger/v2"
)

var (
	host       string
	force      bool
	service    string
	debug      bool
	configPath string
)

func init() {
	rand.Seed(time.Now().Unix())
}

func main() {
	rootCmd := &cobra.Command{
		Use:               "esa",
		SilenceErrors:     true,
		SilenceUsage:      true,
		PersistentPreRunE: PreRun,
		RunE:              Run,
	}

	rootCmd.AddCommand(indices.Command())
	rootCmd.AddCommand(reindex.Command())
	rootCmd.AddCommand(custom.Command())
	rootCmd.AddCommand(bulk.Command())
	rootCmd.AddCommand(tasks.Command())

	rootCmd.PersistentFlags().StringVar(&host, "host", "", "elasticsearch instance")
	rootCmd.PersistentFlags().StringVar(&service, "service", "", "elasticsearch configuration service name")
	rootCmd.PersistentFlags().StringVar(&configPath, "config", "", "elasticsearch configuration toml file path")
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "enables debug mode")
	rootCmd.PersistentFlags().BoolVar(&force, "force", false, "ignored read only property")

	if err := rootCmd.Execute(); err != nil {
		rootCmd.Println("ERR:", errors.Collate(err))
		os.Exit(1)
	}
}

// Run runs command.
func Run(cmd *cobra.Command, args []string) error {
	es, err := infrastructure.NewElasticSearchClient(infrastructure.SessionConfiguration.Host)
	if err != nil {
		return errors.Wrap(err, "failed to create client")
	}

	res, err := es.Info(
		es.Info.WithHuman(),
	)
	if err != nil {
		return errors.Wrap(err, "failed to cat info")
	}

	defer res.Body.Close()

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read response body")
	}

	cmd.Println(string(raw))

	return nil
}

// PreRun pre runs command.
func PreRun(cmd *cobra.Command, args []string) error {
	logger.DebugMode(debug)

	if host == "" && service == "" {
		return fmt.Errorf("you need to specify either host or service name")
	}

	if host != "" {
		infrastructure.SessionConfiguration.Host = host
	} else if configPath != "" {
		configs, path, err := infrastructure.LoadConfigurationFromFile(configPath)
		if err != nil {
			return errors.Wrap(err, "failed to load services")
		}

		c, exists := configs[service]
		if !exists {
			return fmt.Errorf("service %s does not exists", service)
		}

		infrastructure.SessionConfiguration = c
		logger.Debug().Write("configuration found in: %s", path)
	} else if service != "" {
		configs, path, err := infrastructure.LoadConfiguration()
		if err != nil {
			return errors.Wrap(err, "failed to load services")
		}

		c, exists := configs[service]
		if !exists {
			return fmt.Errorf("service %s does not exists", service)
		}

		infrastructure.SessionConfiguration = c
		logger.Debug().Write("configuration found in: %s", path)
	}

	if force {
		infrastructure.SessionConfiguration.ReadOnly = false
	}

	if err := validator.Validate(infrastructure.SessionConfiguration); err != nil {
		return errors.Wrap(err, "invalid parameters")
	}

	return nil
}
