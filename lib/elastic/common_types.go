package elastic

import "encoding/json"

// DocumentResponse elastic document response model.
type DocumentResponse struct {
	Index   string          `json:"_index"`
	Type    string          `json:"_type"`
	ID      string          `json:"_id"`
	Version int             `json:"_version"`
	Found   bool            `json:"found"`
	Source  json.RawMessage `json:"_source"`
}

// GetSource unmarshals source.
func (r *DocumentResponse) GetSource(dst interface{}) error {
	return json.Unmarshal(r.Source, dst)
}

// UpdateRequest update request with upsert options.
type UpdateRequest struct {
	Doc         interface{} `json:"doc"`
	DocAsUpsert bool        `json:"doc_as_upsert"`
}

// BulkUpdateResponse bulk update response model.
type BulkUpdateResponse struct {
	Errors bool `json:"errors"`
	Items  []map[string]struct {
		ID    string `json:"_id"`
		Error struct {
			Reason string `json:"reason"`
		} `json:"error"`
	} `json:"items"`
}
