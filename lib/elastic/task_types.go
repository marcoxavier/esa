package elastic

import "encoding/json"

// WaitForCompletionResponse wait for completion response model.
type WaitForCompletionResponse struct {
	Task string `json:"task"`
}

// TasksGetResponse response model.
type TasksGetResponse struct {
	Completed bool `json:"completed"`
	Task      struct {
		Status json.RawMessage `json:"status"`
	} `json:"task"`
	Response json.RawMessage `json:"response"`
	Errors   json.RawMessage `json:"errors"`
}

// GetStatus unmarshals status.
func (r *TasksGetResponse) GetStatus(status interface{}) error {
	return json.Unmarshal(r.Task.Status, status)
}

// GetResponse unmarshals response.
func (r *TasksGetResponse) GetResponse(response interface{}) error {
	return json.Unmarshal(r.Response, response)
}

// HasErrors checks if tasks has errors.
func (r *TasksGetResponse) HasErrors() bool {
	return r.Errors != nil
}

// GetErrors unmarshals errors.
func (r *TasksGetResponse) GetErrors(errors interface{}) error {
	return json.Unmarshal(r.Errors, errors)
}
