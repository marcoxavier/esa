package elastic

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	errors "gitlab.com/vredens/go-errors"
)

// GetTask gets task info.
func (c *Client) GetTask(ctx context.Context, id string) (TasksGetResponse, error) {
	var response TasksGetResponse

	raw, err := c.GetTaskRaw(ctx, id)
	if err != nil {
		return response, err
	}

	if err := json.Unmarshal(raw, &response); err != nil {
		return response, errors.Wrap(err, "failed to unmarshal response")
	}

	return response, nil
}

// GetTaskRaw gets task raw info.
func (c *Client) GetTaskRaw(ctx context.Context, id string) ([]byte, error) {
	res, err := c.Tasks.Get(
		id,
		c.Tasks.Get.WithPretty(),
		c.Tasks.Get.WithContext(ctx),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get task status by id")
	}

	defer res.Body.Close()

	if res.IsError() {
		return nil, errors.Wrap(fmt.Errorf(res.String()), "failed to get task status")
	}

	raw, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read response")
	}

	return raw, nil
}

// WatchTaskOptions contains all optional parameters to task watcher.
type WatchTaskOptions struct {
	ID       string
	Interval time.Duration
}

func (o *WatchTaskOptions) setDefaults() {
	if o.Interval == 0 {
		o.Interval = time.Second
	}
}

// WatchTask starts watching task.
func (c *Client) WatchTask(ctx context.Context, options WatchTaskOptions, callback func(TasksGetResponse) error) error {
	options.setDefaults()

	for {
		response, err := c.GetTask(ctx, options.ID)
		if err != nil {
			return errors.Wrap(err, "failed to get task")
		}

		if err := callback(response); err != nil {
			return err
		}

		if response.Completed {
			return nil

		}

		select {
		case <-ctx.Done():
			return nil
		case <-time.After(time.Second * 2):
			continue
		}
	}
}
