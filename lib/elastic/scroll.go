package elastic

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	errors "gitlab.com/vredens/go-errors"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
)

// ScrollOptions contains all optional parameters to scroll.
type ScrollOptions struct {
	ScrollID string
	Query    string
	Size     int
}

func (o *ScrollOptions) setDefaults() {
	if o.Size == 0 {
		o.Size = 1000
	}
}

// Scroll is a elastic scroll helper to abstract all necessary calls.
type Scroll struct {
	client    *elasticsearch.Client
	indexName string
	options   ScrollOptions
	err       error
	response  ScrollResponse
	mux       sync.Mutex
}

// NewScroll creates a new elastic scroll.
func (c *Client) NewScroll(indexName string, options ScrollOptions) *Scroll {
	options.setDefaults()

	return &Scroll{
		client:    c.Client,
		indexName: indexName,
		options:   options,
		mux:       sync.Mutex{},
	}
}

// Next check if there is next values to be returned by Scan or GetResponse functions.
// Usually this used as a loop break condition.
func (s *Scroll) Next() bool {
	s.mux.Lock()
	defer s.mux.Unlock()

	if s.hasNext() {
		return true
	}

	s.response = ScrollResponse{}
	s.err = nil

	if s.options.ScrollID == "" {
		s.start()
		return s.hasNext()
	}

	s.iterate()
	return s.hasNext()
}

// Scan scans scroll response hit source to destination parameter.
func (s *Scroll) Scan(dst interface{}) error {
	s.mux.Lock()
	defer s.mux.Unlock()

	if s.err != nil {
		return s.err
	}

	var hit = s.response.Hits.Hits[0]
	s.response.Hits.Hits = s.response.Hits.Hits[1:]

	return hit.GetSource(dst)
}

// ScanBatch scans scroll response hits sources to destination parameter.
func (s *Scroll) ScanBatch(dst interface{}) error {
	s.mux.Lock()
	defer s.mux.Unlock()

	if s.err != nil {
		return s.err
	}

	res := s.response
	s.response = ScrollResponse{}

	return res.GetSources(dst)
}

// GetResponse returns scroll response.
func (s *Scroll) GetResponse() (ScrollResponse, error) {
	s.mux.Lock()
	defer s.mux.Unlock()

	res := s.response
	s.response = ScrollResponse{}

	return res, s.err
}

func (s *Scroll) start() {
	res, err := s.client.Search(
		s.client.Search.WithBody(strings.NewReader(s.options.Query)),
		s.client.Search.WithSize(s.options.Size),
		s.client.Search.WithIndex(s.indexName),
		s.client.Search.WithScroll(time.Minute),
	)
	if err != nil {
		s.err = errors.Wrap(err, "failed scrolling index")
		return
	}

	defer res.Body.Close()

	if res.IsError() {
		s.err = errors.Wrap(fmt.Errorf(res.String()), "failed scrolling index")
		return
	}

	if err := json.NewDecoder(res.Body).Decode(&s.response); err != nil {
		s.err = errors.Wrap(err, "failed to decode response")
		return
	}

	s.options.ScrollID = s.response.ScrollID

	return
}

func (s *Scroll) iterate() {
	res, err := s.client.Scroll(
		s.client.Scroll.WithScroll(time.Minute),
		s.client.Scroll.WithScrollID(s.options.ScrollID),
	)
	if err != nil {
		s.err = errors.Wrap(err, "failed scrolling index")
		return
	}

	defer res.Body.Close()

	if res.IsError() {
		s.err = errors.Wrap(fmt.Errorf(res.String()), "failed scrolling index")
		return
	}

	if err := json.NewDecoder(res.Body).Decode(&s.response); err != nil {
		s.err = errors.Wrap(err, "failed to decode response")
		return
	}

	return
}

func (s *Scroll) hasNext() bool {
	return s.err != nil || len(s.response.Hits.Hits) > 0
}
