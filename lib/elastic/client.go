package elastic

import (
	validator "gopkg.in/validator.v2"

	elasticsearch "github.com/elastic/go-elasticsearch/v8"
	errors "gitlab.com/vredens/go-errors"
)

// Client is a elastic client wrapper.
type Client struct {
	*elasticsearch.Client
	config ClientConfig
}

// ClientConfig contains all parameters to client.
type ClientConfig struct {
	Addresses []string
}

// NewClient creates a elastic client.
func NewClient(config ClientConfig) (*Client, error) {
	if err := validator.Validate(config); err != nil {
		return nil, errors.Wrap(err, "invalid configuration")
	}

	es, err := elasticsearch.NewClient(elasticsearch.Config{Addresses: config.Addresses})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create elasticsearch client")
	}

	return &Client{
		Client: es,
		config: config,
	}, nil
}
