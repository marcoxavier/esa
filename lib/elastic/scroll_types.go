package elastic

import "encoding/json"

// ScrollResponse elastic scroll response model.
type ScrollResponse struct {
	ScrollID string `json:"_scroll_id"`
	Hits     struct {
		Total int                `json:"total"`
		Hits  []DocumentResponse `json:"hits"`
	} `json:"hits"`
}

// GetSources unmarshals sources.
func (r *ScrollResponse) GetSources(dst interface{}) error {
	var sources = make([]json.RawMessage, 0, len(r.Hits.Hits))
	for _, hit := range r.Hits.Hits {
		sources = append(sources, hit.Source)
	}

	raw, err := json.Marshal(sources)
	if err != nil {
		return err
	}

	return json.Unmarshal(raw, dst)
}
