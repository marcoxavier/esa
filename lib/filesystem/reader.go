package filesystem

import (
	"io/ioutil"
	"os"
)

// LoadFile reads the entire contents of a file into a byte slice.
func LoadFile(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return data, nil
}
